# Yii2 extension that allows to create backups of mysql database (structure and data) 
## Versatile setup and preparation for export and import to external databases.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist fadeevms/yii2-dump-db "*"
```

or add

```json
"fadeevms/yii2-dump-db": "*"
```

to the require section of your composer.json.


To use this extension,  simply add the following code in your application configuration:

```php
    'components' => [
        ...
        'dumper' => [
            'class' => 'fadeevms\dump\dumpDB',
        ],
        ...
    ],
```

###Usage:
```
  $dumper = \Yii::$app->dumper;
  echo $dumper->getDump();
```
###method $dumper->getDump() options
  - ['filename' => 'name.sql', 'tables' => ['{{%tb1}}', '{{%tb2}}'], 'newPrefix' => "prefix_"] or
    ['exclude_tables' => ['{{%tb1}}', '{{%tb2}}']]
```
  $dumper->getDump('false', [
    'tables' => [
        '{{%table_one}}',
        '{{%table_two}}'
    ],
    ## if 'tables' is set, 'exclude_tables' is ignored
    // 'exclude_tables' => [
    //    '{{%table_1}}', 
    //    '{{%table_10}}'
    // ],
    ## Dumps with current prefix if 'newPrefix' is not set.
    'newPrefix' => 'glb_',
  ]);
    
```

###Saving the dump to a file:

          $dumper = \Yii::$app->dumper;
          $bk_file = 'FILE_NAME-'.date('YmdHis').'.sql';
          $fh = fopen($bk_file, 'w') or die("can't open file");
          fwrite($fh, $dumper->getDump(FALSE));
          fclose($fh);

###Dumping external DB with SSL (Optional, simply remove the attributes property)
```
  $db = new \yii\db\Connection([
      'dsn' => $dsn,
      'username' => $username,
      'password' => $password,
      'charset' => "utf8",
      "attributes" => [ 
              PDO::MYSQL_ATTR_SSL_KEY => "/path/to/the/client-key.pem", 
              PDO::MYSQL_ATTR_SSL_CERT => "/path/to/the/client-cert.pem", 
              PDO::MYSQL_ATTR_SSL_CA => "/path/to/the/server-ca.pem", 
      ],                
  ]);

  $dumper = new \fadeevms\dump\dumpDB($db);
  $dumper->setRemoveViewDefinerSecurity(TRUE);
  echo $dumper->getDump();
  
```

###Sample
```
  $source_db = Yii::$app->adm_01;
  $dumper = new \app\modules\db_dumper\dumpDB($source_db);
  $dumper->setRemoveViewDefinerSecurity(TRUE);
  $dumped = $dumper->getDump(false, [
      'tables' => [
          '{{%language_source}}',
          '{{%language_message}}'
      ],
      // if table isn't empty, exclude_tables is ignored
      // 'exclude_tables' => ['{{%table}}'],
      'newPrefix' => 'glb_',
  ]);
  
  $dumped_path = "/PATH/TO/DUMP/DIR/";
  $dumped_filename = 'FILENAME_THINGGY'.date('YmdHis').'.sql';
  $theDumpFile = fopen($dumped_path . $dumped_filename, "w") or die("Unable to open Dump file");
  fwrite($theDumpFile, $dumped);
  fclose($theDumpFile);

  exec("mysql -h HOSTNAME/IP -u USER --password=PASSWORD --ssl-key=/PATH/TO/SSL/client-key.pem --ssl-cert=/PATH/TO/SSL/client-cert.pem --ssl-ca=/PATH/TO/SSL/server-ca.pem glb_lng_db_01 < " . $dumped_path . $dumped_filename . " 2>&1", $output);
  $output[] = $output;

  print_r($output);
```